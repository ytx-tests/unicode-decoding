#include <cstring>
#include <iostream>

#include <unicode/ucnv.h>

// encoding
static constexpr char kEncoding[] = "UTF-8";

// source data 1
static constexpr uint8_t kBuf1[] = { 0xEF, 0xBB, 0xBF, 0x00 };
static const char *const kBuf1Start = reinterpret_cast<const char*>(&kBuf1[0]);
static const char *const kBuf1Limit = kBuf1Start + strlen(kBuf1Start);
static constexpr UBool kFlushBuf1 = false;

// source data 2
static constexpr uint8_t kBuf2[] = { 0x40, 0x00 };
static const char *const kBuf2Start = reinterpret_cast<const char*>(&kBuf2[0]);
static const char *const kBuf2Limit = kBuf2Start + strlen(kBuf2Start);
static constexpr UBool kFlushBuf2 = true;

// target
static constexpr size_t kTargetBufSize = 128;

// print utilities

static void print_line(std::ostream& output, const char *line) {
    output << line << "\n";
}

static void print_ptr(std::ostream& output, const char *description, const void *addr) {
    output << description << addr << "\n";
}

static void print_uchar_array(std::ostream& output, const UChar *start, const UChar *end) {
    // start: inclusive; end: exclusive
    print_line(output, "[UChar array]");
    for (const uint8_t *i = reinterpret_cast<const uint8_t*>(start);
            i < reinterpret_cast<const uint8_t*>(end); ++i) {
        output << (i - reinterpret_cast<const uint8_t*>(start))
                << " (" << reinterpret_cast<const void*>(i) << ") : 0x"
                << std::hex << static_cast<uint16_t>(*i) << std::dec << "\n";
    }
}

static void print_err(std::ostream& output, const UErrorCode& err) {
    output << "UErrorCode: " << static_cast<int16_t>(err) << "\n";
}

// main

int main() {

    // Initialize target

    UChar target_buf[kTargetBufSize];
    std::memset(target_buf, 0, sizeof(target_buf));

    UChar *target = &target_buf[0];
    UChar *target_start = target;
    UChar *target_limit = target + kTargetBufSize;

    print_line(std::cout, "[Initial]");
    print_ptr(std::cout, "Target: ", target);
    print_ptr(std::cout, "Target limit: ", target_limit);

    // Initialize source

    const char *source = kBuf1Start;
    const char *source_limit = kBuf1Limit;

    print_ptr(std::cout, "Source: ", source);
    print_ptr(std::cout, "Source limit: ", source_limit);
    print_line(std::cout, "");

    // Initialize converter
    UErrorCode err = U_ZERO_ERROR;
    UConverter *converter = ucnv_open(kEncoding, &err);

    print_line(std::cout, "[Opened converter]");
    print_err(std::cout, err);
    bool failed = err > 0;
    print_line(std::cout, failed ? "(failed)" : "(succeeded)");
    print_line(std::cout, "");

    if (failed) {
        exit(1);
    }

    // First conversion

    UBool flush = kFlushBuf1;
    err = U_ZERO_ERROR;
    ucnv_toUnicode(converter, &target, target_limit,
        &source, source_limit, nullptr, flush, &err);

    print_line(std::cout, "[After first conversion]");
    print_err(std::cout, err);
    print_ptr(std::cout, "Target: ", target);
    print_ptr(std::cout, "Target limit: ", target_limit);
    print_ptr(std::cout, "Source: ", source);
    print_ptr(std::cout, "Source limit: ", source_limit);
    print_line(std::cout, "");

    print_uchar_array(std::cout, target_start, target);
    print_line(std::cout, "");

    // Prepare for second conversion

    source = kBuf2Start;
    source_limit = kBuf2Limit;

    print_line(std::cout, "[Prepared data for second conversion]");
    print_ptr(std::cout, "Target: ", target);
    print_ptr(std::cout, "Target limit: ", target_limit);
    print_ptr(std::cout, "Source: ", source);
    print_ptr(std::cout, "Source limit: ", source_limit);
    print_line(std::cout, "");

    // Second conversion

    flush = kFlushBuf2;
    err = U_ZERO_ERROR;
    ucnv_toUnicode(converter, &target, target_limit,
        &source, source_limit, nullptr, flush, &err);

    print_line(std::cout, "[Finished second conversion]");
    print_err(std::cout, err);
    print_ptr(std::cout, "Target: ", target);
    print_ptr(std::cout, "Target limit: ", target_limit);
    print_ptr(std::cout, "Source: ", source);
    print_ptr(std::cout, "Source limit: ", source_limit);
    print_line(std::cout, "");

    print_uchar_array(std::cout, target_start, target);
    print_line(std::cout, "");

    // Close converter

    ucnv_close(converter);

    return 0;
}