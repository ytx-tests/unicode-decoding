const { Buffer } = require('node:buffer');

const kEncoding = 'UTF-8';

/**
 * @param { boolean } ignoreBOM
 */
function decodeTest(ignoreBOM) {
    const decoder = new TextDecoder(kEncoding, {
        ignoreBOM: ignoreBOM
    });

    let result = decoder.decode(Buffer.from([0xEF, 0xBB]), { stream: true });
    result = decoder.decode(Buffer.from([0xBF, 0x40]), { stream: false });

    console.info(`Decoded string: "${result}", length: ${result.length}`);
    for (let i = 0; i < result.length; ++i) {
        console.log(i, `: ${result.codePointAt(i).toString(16)}`);
    }
    console.log('');
}

decodeTest(false);
decodeTest(true);